# ---Stdlib---
import sys
import os
from pathlib import Path
from typing import List

# ---Dependencies---
import torch
from imageio import imread
import matplotlib.pyplot as plt

from packages.SAND_features.utils import ops
from packages.SAND_features.models import Sand


def sand_function(model_name: str, image_path: str, output_path: str, img_index_range: List[int] = None) -> None:
    root = Path(__file__) .parent  # Path to repo
    if root not in sys.path:
        sys.path.insert(0, root)  # Prepend to path so we can use these modules

    model_path = root/'ckpts'

    device = ops.get_device()

    ckpt_file = Path(model_path, model_name).with_suffix('.pt')

    images = []
    for filename in os.listdir(image_path):
        if filename.endswith('.png'):
            images.append(filename)

    images.sort()

    start_point = 0
    end_point = len(images) - 1
    if img_index_range:
        start_point = img_index_range[0]
        end_point = img_index_range[1]

    for i in range(start_point, end_point + 1):
        print(f'Processing {images[i]}.')
        img_file = Path(image_path + '/' + images[i])

        # Load image & convert to torch format
        img_np = imread(img_file)
        img_torch = ops.img2torch(img_np, batched=True).to(device)

        # Create & load model (single branch)
        model = Sand.from_ckpt(ckpt_file).to(device)
        model.eval()

        # Run inference
        with torch.no_grad():
            features_torch = model(img_torch)

        # Convert features into an images we can visualize (by PCA or normalizing)
        features_np = ops.fmap2img(features_torch).squeeze(0)
        plt.imsave(output_path + '/' + images[i], features_np)
