import subprocess


class VideoProcessing:
    def __init__(self, video_name, unprocessed_path, processed_path):
        self.video_name = video_name
        self.unprocessed_path = unprocessed_path
        self.processed_path = processed_path

    def split_video(self) -> None:
        video_no_type = self.video_name.split('.')[0]
        unprocessed_video_path = self.unprocessed_path + '/' + video_no_type

        subprocess.run(['mkdir', video_no_type], cwd=self.unprocessed_path)
        subprocess.run(['mv', self.video_name, video_no_type],
                       cwd=self.unprocessed_path)
        subprocess.run(['mkdir', video_no_type], cwd=self.processed_path)
        subprocess.run(['ffmpeg', '-i', self.video_name, 'frame%04d.png',
                        '-hide_banner'], cwd=unprocessed_video_path)

    def combine_images(self):
        video_no_type = self.video_name.split('.')[0]
        processed_video_path = self.processed_path + '/' + video_no_type

        subprocess.run(['ffmpeg', '-framerate', '30', '-i', 'frame%04d.png', '-pix_fmt', 'yuv420p',
                        self.video_name], cwd=processed_video_path)
